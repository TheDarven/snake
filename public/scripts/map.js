let EMPTY = 0;
let WALL = 1;
let FOOD = 2;
let ICE = 3;
let SNAKE = 4;
let CANNON = 5;
let CANNON_BALL = 6;

let boxSize = 17;
let borderSize = 2;

let colors = {
    backgroundOutisde: "#9AC503",
    backgroundInside: "#789903",
    wall: "#8AAF03",
    ice: "#7594CB",
    snakeHead: "#181E00",
    snakeBody: "#000000",
    foodDefault: "#D14A03",
    foodShrink: "#EFC050",
    score: "#FFFFFF",
    cannon: "#9AA09A",
    cannonText: "#FFFFFF",
    cannonBall: "#000000"
};

let musics = {
    snk1: {file: "assets/musics/snk1.mp3", start: 0.6, volumeMultiply: 1},
    snk2: {file: "assets/musics/snk2.mp3", start: 0, volumeMultiply: 1},
    watc: {file: "assets/musics/weAreTheChampion.mp3", start: 5.2, volumeMultiply: 1.2},
    gyu: {file: "assets/musics/giveYouUp.mp3", start: 0, volumeMultiply: 1},
    ambiance: {file: "assets/musics/ambiance.mp3", start: 3.6, volumeMultiply: 1}
};

// La configuration de début de la partie
let mapOptions = {
    dimensions: [10, 10],

    delay: 200,
    bonus: 0,
    maxSpeed: 10,
    defaultFood: "default",
    foodProbaDefault: 90,
    foodProbaShrink: 10,

    songs: [],

    walls: [],
    ice: [],
    food: [
        [5, 5]
    ],
    snake: [1, 1],
    cannon: []
};

// Les valeurs qui changent au cours de la partie
let mapElements = {
    world: [],
    snake: [],
    food: [],
    cannon: [],
    score: -1
};

/**
 * [UTIL]
 * Sauvegarde en mémoire la carte selectionnée dans le jeu
 *
 * @param jsonMap La carte au format JSON
 */
function selectMap(jsonMap){
    mapOptions.dimensions = jsonMap["dimensions"];

    mapOptions.delay = jsonMap["delay"];
    mapOptions.bonus = isNaN(jsonMap["bonus"]) ? 0 : jsonMap["bonus"];
    mapOptions.maxSpeed = isNaN(jsonMap["maxSpeed"]) ? 10 : jsonMap["maxSpeed"];
    mapOptions.defaultFood = jsonMap["defaultFood"] === undefined ? "default" : jsonMap["defaultFood"];
    mapOptions.foodProbaDefault = isNaN(jsonMap["foodProbaDefault"]) ? 90 : jsonMap["foodProbaDefault"];
    mapOptions.foodProbaShrink = isNaN(jsonMap["foodProbaShrink"]) ? (100-mapOptions.foodProbaDefault) : jsonMap["foodProbaShrink"];

    mapOptions.songs = isNaN(jsonMap["songs"]) ? ["snk1", "watc", "snk2", "gyu"] : jsonMap["songs"];

    mapOptions.walls = jsonMap["walls"];
    mapOptions.ice = jsonMap["ice"] === undefined ? [] : jsonMap["ice"];
    mapOptions.food = jsonMap["food"] === undefined ? [] : jsonMap["food"];
    mapOptions.snake = jsonMap["snake"];
    mapOptions.cannon = jsonMap["cannon"] === undefined ? [] : jsonMap["cannon"];

    mapElements.world = Array.from(Array(mapOptions.dimensions[1]), () => Array.from(Array(mapOptions.dimensions[0]), () => EMPTY));
    mapElements.snake = [];
    mapElements.food = [];
    mapElements.cannon = [];
    mapElements.score = 0;

    for(let i=0; i<mapOptions.ice.length; i++){
        let ice = mapOptions.ice[i];
        mapElements.world[ice[1]][ice[0]] = ICE;
    }
    for(let i=0; i<mapOptions.walls.length; i++){
        let wall = mapOptions.walls[i];
        mapElements.world[wall[1]][wall[0]] = WALL;
    }
    for(let i=0; i<mapOptions.cannon.length; i++){
        let cannon = mapOptions.cannon[i];
        mapElements.world[cannon[1]][cannon[0]] = CANNON;
        mapElements.cannon.push([cannon[0], cannon[1], cannon[2], [cannon[0], cannon[1]]]);
    }
    for(let i=0; i<mapOptions.snake.length; i++){
        let snake = mapOptions.snake[i];
        mapElements.world[snake[1]][snake[0]] = SNAKE;
        mapElements.snake.push(snake);
    }
    if(mapOptions.food.length === 0 || mapOptions.food[0].length === 0){
        if(changeFoodPosition(true))
            return;
    }else{
        mapElements.food[0] = [ mapOptions.food[0][0], mapOptions.food[0][1], (mapOptions.food[0].length < 3 ? mapOptions.defaultFood : mapOptions.food[0][2]) ];
    }
    let food = mapElements.food[0];
    mapElements.world[food[1]][food[0]] = FOOD;

    openGamePage();
    drawGame();

    gameListener = setInterval(function(){
        step();
    }, mapOptions.delay);
}

/**
 * [UTIL]
 * Pour vider le canvas
 */
function drawBackground(){
    gameContext.fillStyle = colors.backgroundOutisde;
    gameContext.fillRect(0, 0, gameCanvas.width, gameCanvas.height);
}

/**
 * [UTIL]
 * Pour afficher la partie
 * Quand : A chaque tick de jeu et au lancement
 */
function drawGame(){
    calculBoxSize(mapOptions.dimensions[0]+2, mapOptions.dimensions[1]+10);
    drawBackground();
    let xCenter = gameCanvas.width/2;
    let yCenter = gameCanvas.height/2;

    // La bordure de la zone de jeu
    let topX = xCenter-boxSize*(mapElements.world[0].length/2+borderSize);
    let topY = yCenter-boxSize*(mapElements.world.length/2+borderSize);
    let width = (mapElements.world[0].length+borderSize*2)*boxSize;
    let height = (mapElements.world.length+borderSize*2)*boxSize;
    gameContext.fillStyle = colors.wall;
    gameContext.fillRect(topX, topY, width, height);

    // La zone de jeu
    topX = xCenter-boxSize*(mapElements.world[0].length/2);
    topY = yCenter-boxSize*(mapElements.world.length/2);
    width = mapElements.world[0].length*boxSize;
    height = mapElements.world.length*boxSize;
    gameContext.fillStyle = colors.backgroundInside;
    gameContext.fillRect(topX, topY, width, height);

    for(let y=0; y<mapElements.world.length; y++){
        for(let x=0; x<mapElements.world[y].length; x++){
            let element = mapElements.world[y][x];
            if(element === FOOD || hasElement(mapElements.food, x, y)){
                if(mapElements.food[0][2] === "default")
                    gameContext.fillStyle = colors.foodDefault;
                else if(mapElements.food[0][2] === "shrink")
                    gameContext.fillStyle = colors.foodShrink;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
            }else if(element === WALL){
                gameContext.fillStyle = colors.wall;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
            }else if(element === SNAKE){
                if(x === mapElements.snake[mapElements.snake.length-1][0] && y === mapElements.snake[mapElements.snake.length-1][1]) {
                    gameContext.fillStyle = colors.snakeHead;
                    gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);

                    gameContext.fillStyle = colors.backgroundOutisde;
                    let xScale = boxSize/5;
                    let yScale = boxSize/3;
                    gameContext.fillRect(topX+x*boxSize+xScale, topY+y*boxSize+yScale, xScale, yScale);
                    gameContext.fillRect(topX+x*boxSize+xScale*3, topY+y*boxSize+yScale, xScale, yScale);
                }else{
                    gameContext.fillStyle = colors.snakeBody;
                    gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
                }
            }else if(element === ICE || hasElement(mapOptions.ice, x, y)){
                gameContext.fillStyle = colors.ice;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
            }else if(element === CANNON){
                gameContext.fillStyle = colors.cannon;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);

                gameContext.fillStyle = colors.cannonText;
                gameContext.font = (boxSize*0.7)+"px Arial";
                gameContext.textAlign = "center";
                gameContext.textBaseline = "middle";

                let cannon = getCannon(mapElements.cannon, x, y);
                let letter = cannon === null ? "U" : getCannonLetter(cannon);

                gameContext.fillText(letter, topX+(x+0.5)*boxSize, topY+(y+0.5)*boxSize);
            }
            if(element === CANNON_BALL){
                gameContext.fillStyle = colors.cannonBall;
                gameContext.beginPath();
                gameContext.arc(topX+(x+0.5)*boxSize,topY+(y+0.5)*boxSize,boxSize/2.2,0*Math.PI,2*Math.PI);
                gameContext.fill();
            }
        }
    }
    gameContext.fillStyle = colors.score;
    gameContext.textAlign = "center";
    gameContext.textBaseline = "middle";
    gameContext.font = "45px Arial";
    gameContext.fillText("Score: "+mapElements.score, xCenter, topY/2);
}




/**
 * [UTIL]
 * Pour mettre à jour boxSize avec la valeur la plus grande possible
 *
 * @param width La longyeur de la zone à afficher
 * @param height La largeur de la zone à afficher
 */
function calculBoxSize(width, height){
    boxSize = 30;
    while ((boxSize*width > gameCanvas.width || boxSize*height > gameCanvas.height) && boxSize > 3){
        boxSize--;
    }
}

/**
 * [SOUND]
 * Pour jouer le son (insupportable) du menu principal
 */
function mainSound(enable){
    let soundElement = document.getElementById("mainSound");
    if(soundElement !== undefined){
        if(enable){
            soundElement.currentTime = 0;
            soundElement.volume = options.menuLevel;
            soundElement.loop = true;
            soundElement.play();
        }else{
            soundElement.pause();
        }
    }
}

/**
 * [SOUND]
 * Pour jouer le son de fond du jeu
 */
function playSound(enable){
    let soundElement = document.getElementById("playSound");
    if(soundElement !== undefined){
        if(enable){
            let randomMusic = Math.floor((Math.random() * mapOptions.songs.length));
            let selectedMusic = musics[mapOptions.songs[randomMusic]];

            soundElement.getElementsByTagName("source")[0].src = selectedMusic.file;
            soundElement.load();
            soundElement.currentTime = selectedMusic.start;
            soundElement.volume = options.musicLevel*selectedMusic.volumeMultiply;
            soundElement.play();
        }else{
            soundElement.pause();
        }
    }
}

/**
 * [SOUND]
 * Pour jouer le son de la mort
 */
function deathSound(){
    let soundElement = document.getElementById("deathSound");
    if(soundElement !== undefined){
        soundElement.currentTime = 0.6;
        soundElement.volume = options.deathLevel;
        soundElement.loop = false;
        soundElement.play();
    }
}

/**
 * [SOUND]
 * Crunnch, c'est le bruit de la pomme qui se fait manger
 */
function eatSound(){
    let soundElement = document.getElementById("eatSound");
    if(soundElement !== undefined){
        soundElement.currentTime = 1;
        soundElement.volume = options.eatLevel;
        soundElement.loop = false;
        soundElement.play();
    }
}

/**
 * [UTIL]
 * Pour savoir si il y a  un élément dit à des coordonnées
 *
 * @param list La liste dans laquelle chercher
 * @param x L'axe x
 * @param y L'axe y
 */
function hasElement(list, x, y){
    for(let i=0; i<list.length; i++){
        let element = list[i];
        if(element[0] === x && element[1] === y)
            return true;
    }
    return false;
}

/**
 * Pour avoir un canon à des coordonnées
 *
 * @param list La liste dans laquelle chercher
 * @param x L'axe x
 * @param y L'axe y
 * @returns {null|*} L'element qui contient le canon, null sinon
 */
function getCannon(list, x, y){
    for(let i=0; i<list.length; i++){
        let element = list[i];
        if(element[0] === x && element[1] === y)
            return element;
    }
    return null;
}

function getCannonLetter(element){
    let letters = {top: "H", right: "D", bottom: "B", left: "G"};
    return letters[element[2]];
}
