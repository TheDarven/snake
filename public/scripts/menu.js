let menuHTML = {
    HTMLElement: null,
    map : {HTMLElement: null, value: ""},
    play: {HTMLElement: null},
    editor: {HTMLElement: null},
    option: {HTMLElement: null},
    delete: {HTMLElement: null}
};

let options = {
    menuLevel: 0.3,
    musicLevel: 0.3,
    deathLevel: 0.3,
    eatLevel: 0.5
};

let mapUrl = "https://thedarven.gitlab.io/snake/data/";

let mapList = [
        ["original", "Original Snake Nokia 6110 ", "server"],
        ["croixception", "Croixception", "server"],
        ["plonger", "Eviter de plonger", "server"],
        ["doublecanon", "Double canon", "server"],
        ["lesboules", "Les boules", "server"]
    ];

/**
 * [LOAD]
 * Initialise les variables du menu
 */
function createMenuVariables(){
    let levelCookie = getCookie("menuLevel");
    if(levelCookie !== undefined && levelCookie.length !== 0)
        options.menuLevel = parseFloat(levelCookie);

    levelCookie = getCookie("musicLevel");
    if(levelCookie !== undefined && levelCookie.length !== 0)
        options.musicLevel = parseFloat(levelCookie);

    levelCookie = getCookie("deathLevel");
    if(levelCookie !== undefined && levelCookie.length !== 0)
        options.deathLevel = parseFloat(levelCookie);

    levelCookie = getCookie("eatLevel");
    if(levelCookie !== undefined && levelCookie.length !== 0)
        options.eatLevel = parseFloat(levelCookie);

    let menuElement = document.getElementById("menu");
    menuHTML.HTMLElement = menuElement;

    loadMapInMenu();

    let menuMaps = document.getElementById("menu-maps");
    let selectedMap = menuMaps.getElementsByClassName("active")[0];
    menuHTML.map.HTMLElement = selectedMap;
    menuHTML.map.value = selectedMap.dataset.mapname;

    let play = document.getElementById("menu-play");
    menuHTML.play.HTMLElement = play;

    let editor = document.getElementById("menu-editor");
    menuHTML.editor.HTMLElement = editor;

    let option = document.getElementById("menu-option");
    menuHTML.option.HTMLElement = option;

    let deleteE = document.getElementById("menu-delete");
    menuHTML.delete.HTMLElement = deleteE;
}

/**
 * [LOAD]
 * Crée les évènements liés au menu
 */
function addMenuEvents(){
    var menuMaps = document.getElementById("menu-maps");
    menuMaps.addEventListener("click", choiceMap);

    menuHTML.play.HTMLElement.addEventListener("click", startGameButton);
    menuHTML.editor.HTMLElement.addEventListener("click", startEditorButton);
    menuHTML.option.HTMLElement.addEventListener("click", startOptionButton);
    menuHTML.delete.HTMLElement.addEventListener("click", deleteOptionButton);
}



/**
 * [EVENT]
 * Lorsqu'on appuie sur une carte
 *
 * @param event L'évènement de click
 */
function choiceMap(event){
    if(event.target !== event.currentTarget && event.target !== menuHTML.map.HTMLElement){
        menuHTML.map.HTMLElement.classList.remove("active");

        menuHTML.map.HTMLElement = event.target;
        menuHTML.map.value = event.target.dataset.mapname;
        menuHTML.map.HTMLElement.classList.add("active");
    }
}

/**
 * [EVENT]
 * Lorsqu'on appuie sur le bouton de démarrage de la partie
 *
 * @param event L'évènement de click
 */
function startGameButton(event){
    if(menuHTML.map.HTMLElement.dataset.localisation === "server"){
        fetch(mapUrl+menuHTML.map.value+".json").then(function (response) {
            if(response.ok){
                return response.json();
            }else{
                throw ("Error in the page loading");
            }
        }).then(function (data) {
            selectMap(data);
        }).catch(function (err) {
            console.log(err);
            return err;
        });
    }else{
        let map = JSON.parse(getCookie("userMap-"+menuHTML.map.value));
        selectMap(map);
    }
}

/**
 * [EVENT]
 * lorsqu'on appuie sur le bouton de démarrage de l'editeur de carte
 *
 * @param event L'évènement de click
 */
function startEditorButton(event) {
    menuHTML.HTMLElement.classList.add("d-none");
    openDimensionEditor();
}

/**
 * [EVENT]
 * Lorsqu'on appuie sur le bouton options
 *
 * @param event L'évènement de click
 */
async function startOptionButton(event){
    const {value: formValues} = await Swal.fire({
        title: 'Options',
        confirmButtonText: "Valider",
        showCloseButton: true,
        html:
            '<div class="form-group mt-3">' +
            '<label for="menuVolum">Volume de l\'accueil</label>' +
            '<input class="form-control-range" id="menuVolum" aria-describedby="menuVolumHelp" type="range" value="'+options.menuLevel+'" step="0.01" min="0" max="1">' +
            '<small id="menuVolumHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '<div class="form-group mt-3">' +
            '<label for="musicVolum">Volume de la musique en jeu</label>' +
            '<input class="form-control-range" id="musicVolum" aria-describedby="musicVolumHelp" type="range" value="'+options.musicLevel+'" step="0.01" min="0" max="1">' +
            '<small id="musicVolumHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '<div class="form-group mt-3">' +
            '<label for="deathVolum">Volume de la mort</label>' +
            '<input class="form-control-range" id="deathVolum" aria-describedby="deathVolumHelp" type="range" value="'+options.deathLevel+'" step="0.01" min="0" max="1">' +
            '<small id="deathVolumHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '<div class="form-group mt-3">' +
            '<label for="eatVolum">Volume de la pomme</label>' +
            '<input class="form-control-range" id="eatVolum" aria-describedby="eatVolumHelp" type="range" value="'+options.eatLevel+'" step="0.01" min="0" max="1">' +
            '<small id="eatVolumHelp" class="form-text text-muted"></small>' +
            '</div>',
        focusConfirm: false,
        preConfirm: () => {
            return [
                document.getElementById('menuVolum') !== null ? document.getElementById('menuVolum').value : null,
                document.getElementById('musicVolum') !== null ? document.getElementById('musicVolum').value : null,
                document.getElementById('deathVolum') !== null ? document.getElementById('deathVolum').value : null,
                document.getElementById('eatVolum') !== null ? document.getElementById('eatVolum').value : null
            ]
        },
    });

    let isValid = false;
    let verifyMessage = "";

    if (formValues){
        if(formValues.length === 4){
            let menuLevel = parseFloat(formValues[0]);
            let musicLevel = parseFloat(formValues[1]);
            let deathLevel = parseFloat(formValues[2]);
            let eatLevel = parseFloat(formValues[3]);
            if(!isNaN(menuLevel) && menuLevel >= 0 && menuLevel <= 1) {
                if (!isNaN(musicLevel) && musicLevel >= 0 && musicLevel <= 1) {
                    if (!isNaN(deathLevel) && deathLevel >= 0 && deathLevel <= 1) {
                        if (!isNaN(eatLevel) && eatLevel >= 0 && eatLevel <= 1) {
                            isValid = true;
                            options.menuLevel = menuLevel;
                            mainSound(true);

                            options.musicLevel = musicLevel;
                            options.deathLevel = deathLevel;
                            options.eatLevel = eatLevel;

                            setCookie("menuLevel", menuLevel, 10);
                            setCookie("musicLevel", musicLevel, 10);
                            setCookie("deathLevel", deathLevel, 10);
                            setCookie("eatLevel", eatLevel, 10);
                        } else {
                            verifyMessage = "Le volume de la pomme n'est pas compris entre 0 et 1."
                        }
                    } else {
                        verifyMessage = "Le volume de la mort n'est pas compris entre 0 et 1."
                    }
                } else {
                    verifyMessage = "Le volume de la musique n'est pas comprise entre 0 et 1.";
                }
            }else{
                verifyMessage = "Le volume du menu n'est pas compris entre 0 et 1.";
            }
        }
    }


    if(!isValid){
        if(verifyMessage !== ""){
            Swal.fire(
                'Erreur',
                verifyMessage,
                'error'
            );
        }
    }

    openMainPage(false);
}

/**
 * [EVENT]
 * Lorsqu'on appuie sur le bouton de suppresion d'une carte
 *
 * @param event L'évènement de click
 */
async function deleteOptionButton(event){
    const {value: formValues} = await Swal.fire({
        title: 'Supprimer une carte',
        confirmButtonText: "Valider",
        showCloseButton: true,
        html:
            '<div class="form-group mt-3">' +
            '<label for="name">Nom de la carte</label>' +
            '<input class="form-control" id="name" aria-describedby="nameHelp" placeholder="Nom de la carte à supprimer">' +
            '<small id="nameHelp" class="form-text text-muted"></small>' +
            '</div>',
        focusConfirm: false,
        preConfirm: () => {
            return [
                document.getElementById('name') !== null ? document.getElementById('name').value : null
            ]
        },
    });

    let isValid = false;
    let verifyMessage = "";

    if (formValues){
        if(formValues.length === 1){
            let mapName = formValues[0];

            if(mapName !== null && mapName.length > 0) {
                for(let i=0; i< mapList.length; i++){
                    if(mapList[i][2] === "local" && mapList[i][1] === mapName){
                        deleteCookie("userMap-"+mapList[i][0]);
                        deleteCookie("score-"+mapList[i][0]);

                        deleteUserMap(mapName);
                        isValid = true;
                        loadMapInMenu();
                        break;
                    }
                }

                if(!isValid)
                    verifyMessage = "Aucune carte ne porte ce nom";
            }else{
                verifyMessage = "Vous n'avez pas entré de nom de carte.";
            }
        }
    }

    if(!isValid){
        if(verifyMessage !== ""){
            Swal.fire(
                'Erreur',
                verifyMessage,
                'error'
            );
        }
        return;
    }

    Swal.fire(
        "Suppresion",
        "La carte a été supprimé avec succès.",
        'success'
    );
}




/**
 * [UTIL]
 * Pour mettre à jour le menu des scores
 *
 * @param victory Si le joueur a gagné
 */
function reloadMenu(victory){
    document.getElementById("menu-result").classList.remove("d-none");
    document.getElementById("menu-result-score").textContent = mapElements.score;
    if(victory)
        document.getElementById("menu-result-title").textContent = "GAGNÉ";
    else
        document.getElementById("menu-result-title").textContent = "PERDU";

    document.getElementById("separator").classList.remove("d-none");

    let score = menuHTML.map.HTMLElement.dataset.best;
    if(score !== undefined){
        if(score >= mapElements.score){
            document.getElementById("menu-result-best").classList.add("d-none");
            return;
        }
    }
    document.getElementById("menu-result-best").classList.remove("d-none");

    menuHTML.map.HTMLElement.dataset.best = mapElements.score;
    menuHTML.map.HTMLElement.getElementsByClassName("menu-maps-score")[0].textContent = "Meilleur score : "+mapElements.score;

    setCookie("score-"+menuHTML.map.HTMLElement.dataset.mapname, mapElements.score, 10000);
}

/**
 * [PAGE]
 * Pour ouvrir la page de l'editeur de carte
 */
function openEditorPage(){
    menuHTML.HTMLElement.classList.add("d-none");
    mainSound(false);

    editorHTML.tools.HTMLElement.classList.remove("d-none");
    editorHTML.config.HTMLElement.classList.remove("d-none");
    state = "editor";
}

/**
 * [PAGE]
 * Pour ouvrir la page du menu principal
 */
function openMainPage(changeSong = true){
    if(changeSong)
        mainSound(true);

    menuHTML.HTMLElement.classList.remove("d-none");

    editorHTML.tools.HTMLElement.classList.add("d-none");
    editorHTML.config.HTMLElement.classList.add("d-none");
    state = "menu";
}

/**
 * [PAGE]
 * Pour ouvrir la page du jeu
 */
function openGamePage(){
    menuHTML.HTMLElement.classList.add("d-none");
    mainSound(false);

    editorHTML.tools.HTMLElement.classList.add("d-none");
    editorHTML.config.HTMLElement.classList.add("d-none");
    state = "game";
}


/**
 * [COOKIE]
 * Pour créer un cookie
 *
 * @param name Le nom du cookie
 * @param value La valeur du cookie
 * @param expirationDays Le nombre de jour de validité du cookie
 */
function setCookie(name, value, expirationDays) {
    let date = new Date();
    date.setTime(date.getTime() + (expirationDays*24*60*60*1000));

    let expires = "expires="+ date.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

/**
 * [COOKIE]
 * Pour récupérer un cookie
 *
 * @param name Le nom du cookie
 * @returns {string} Le contenu du cookie si il existe, une chaîne vide sinon
 */
function getCookie(name) {
    name += "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/**
 * [COOKIE]
 * Pour supprimer un cookie
 *
 * @param name Le nom du cookie à supprimer
 */
function deleteCookie(name){
    setCookie(name, "", 0);
}

function getUserMaps(){
    let userMap = getCookie("userMap");
    if(userMap == null || userMap === undefined || userMap.length === 0)
        return [];
    return JSON.parse(userMap);
}

function saveUserMap(name, content){
    let userMap = getUserMaps();

    let noSpaceName = name;
    noSpaceName = noSpaceName.replace(" ", "-");

    let mapExist = false;
    for(let i=0; i<userMap.length; i++){
        if(userMap[i][0] === noSpaceName){
            userMap[i][1] = name;
            mapExist = true;
            break;
        }
    }
    if(!mapExist){
        userMap.push([noSpaceName, name]);
        setCookie("userMap", JSON.stringify(userMap), 10000000);
    }else{
        deleteCookie("score-"+noSpaceName);
    }
    setCookie("userMap-"+noSpaceName, JSON.stringify(content), 10000000);
}

function deleteUserMap(name){
    let userMap = getUserMaps();
    for(let i=0; i<userMap.length; i++){
        if(userMap[i][1] === name){
            userMap.splice(i, 1);
            break;
        }
    }
    setCookie("userMap", JSON.stringify(userMap), 10000000);
}


function loadMapInMenu(show = true){
    let menuMaps = document.getElementById("menu-maps");
    let child = menuMaps.lastElementChild;
    while(child){
        menuMaps.removeChild(child);
        child = menuMaps.lastElementChild;
    }

    for(let i=0; i<mapList.length; i++){
        if(mapList[i][2] === "local"){
            mapList.splice(i, 1);
            i--;
        }
    }

    let userMaps = getUserMaps();
    for(let i=0; i<userMaps.length; i++)
        mapList.push([userMaps[i][0], userMaps[i][1], "local"]);
    for(let i=0; i<mapList.length; i++){
        let mapButton = document.createElement("button");
        mapButton.classList.add("list-group-item", "list-group-item-action");
        if(i === 0)
            mapButton.classList.add("active");
        mapButton.textContent = mapList[i][1]+" ";
        mapButton.dataset.mapname = mapList[i][0];
        mapButton.dataset.localisation = mapList[i][2];

        let spanScore = document.createElement("span");
        spanScore.classList.add("badge", "badge-pill", "badge-success", "float-right", "menu-maps-score");
        let bestScore = getCookie("score-"+mapList[i][0]);
        if(bestScore != null && bestScore !== undefined && bestScore.length > 0)
            spanScore.textContent = "Meilleur score : "+bestScore;
        mapButton.appendChild(spanScore);

        menuMaps.appendChild(mapButton);

        // <button type="button" class="list-group-item list-group-item-action active" data-mapname="map1">Carte 1 <span class="badge badge-pill badge-success float-right menu-maps-score"></span></button>
    }
    let selectedMap = menuHTML.HTMLElement.getElementsByClassName("active")[0];
    menuHTML.map.HTMLElement = selectedMap;
    menuHTML.map.value = selectedMap.dataset.mapname;
}
