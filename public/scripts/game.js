let gameCanvas;
let gameContext;

let gameListener = null;
let key = null;
let canChangeKey = true;

let lastResize = 0;

let state = "menu";

/**
 * [LOAD]
 * Appel les fonction qui initialisent le jeu
 * Quand : Au chargement de la page
 */
window.addEventListener("load", function (event) {
    createVariables();
    addEvents();
    mainSound(true);
});

/**
 * [LOAD]
 * Initialise les variables
 */
function createVariables(){
    let body = document.getElementsByTagName("body")[0];

    gameCanvas = document.getElementById("snake-canvas");
    gameCanvas.height = document.documentElement.clientHeight;
    gameCanvas.width = body.clientWidth;

    gameContext = gameCanvas.getContext("2d");
    drawBackground();

    createMenuVariables();
    createEditorVariables();
}

/**
 * [LOAD]
 * Crée tout les évènements
 */
function addEvents(){
    window.addEventListener("resize", onScreenResizing);
    window.addEventListener("keydown", onKeyPress);
    document.getElementById("playSound").addEventListener("ended", onMusicEnding);

    addMenuEvents();
    addEditorEvents();
}




/**
 * [EVENT]
 * Lorsqu'on appuie sur une touche du clavier
 *
 * @param event L'évènement de click
 */
function onKeyPress(event){
    if(state === "game" && gameListener !== null && canChangeKey){
        let snakeHead = mapElements.snake[mapElements.snake.length-1];
        // Savoir si il n'y a pas de la glace
        if(!hasElement(mapOptions.ice, snakeHead[0], snakeHead[1])){
            const validKeys = ["ArrowDown", "ArrowUp", "ArrowRight", "ArrowLeft"];
            let isValidMove = false;

            if( (event.key === "ArrowDown" && key !== "ArrowUp") || (event.key === "ArrowUp" && key !== "ArrowDown") ||
                (event.key === "ArrowLeft" && key !== "ArrowRight") || (event.key === "ArrowRight" && key !== "ArrowLeft")  ){

                if(key === null){
                    let newPosition = [mapElements.snake[mapElements.snake.length-1][0], mapElements.snake[mapElements.snake.length-1][1]];
                    if(isCorrectPosition(getNewPosition(newPosition, event.key))){
                        playSound(true);
                        isValidMove = true;
                    }
                }else
                    isValidMove = true;
            }

            if(isValidMove){
                key = event.key;
                canChangeKey = false;
            }
        }
    }
}

/**
 * [EVENT]
 * Lorsqu'on redimensionne la page
 *
 * @param event L'évènement de redimentionnement
 */
function onScreenResizing(event){
    if(lastResize !== null){
        clearTimeout(lastResize);
    }
    lastResize = setTimeout(endOfScreenResizing, 200);
}

/**
 * [EVENT]
 * Lorsque la musique de fond se termine
 *
 * @param event
 */
function onMusicEnding(event){
    if(state === "game"){
        playSound(true);
    }
}






/**
 * [GAME]
 * La fonction tick du jeu dans laquelle on déplace le serpent
 */
function step(){
    if(key === null)
        return;

    let lose = false;
    for(let i=0; i<mapElements.cannon.length; i++){
        let cannon = mapElements.cannon[i];
        let bulletLocation = mapElements.cannon[i][3];

        if(cannon[0] !== bulletLocation[0] || cannon[1] !== bulletLocation[1]){
            if(hasElement(mapElements.food, bulletLocation[0], bulletLocation[1]))
                mapElements.world[bulletLocation[1]][bulletLocation[0]] = FOOD;
            else if (hasElement(mapOptions.ice, bulletLocation[0], bulletLocation[1])){
                mapElements.world[bulletLocation[1]][bulletLocation[0]] = ICE;
            }else{
                mapElements.world[bulletLocation[1]][bulletLocation[0]] = EMPTY;
            }
        }

        let orientation = mapElements.cannon[i][2];
        if(orientation === "top")
            bulletLocation[1] -= 1;
        else if(orientation === "right")
            bulletLocation[0] += 1;
        else if(orientation === "bottom")
            bulletLocation[1] += 1;
        else if(orientation === "left")
            bulletLocation[0] -= 1;

        if(isCorrectPosition(bulletLocation, true)){
            if(mapElements.world[bulletLocation[1]][bulletLocation[0]] === SNAKE){
                lose = true;
                break;
            }else
                mapElements.world[bulletLocation[1]][bulletLocation[0]] = CANNON_BALL;
        }else{
            bulletLocation[0] = cannon[0];
            bulletLocation[1] = cannon[1];
        }
    }

    let newPosition = Array(2);
    newPosition[0] = mapElements.snake[mapElements.snake.length-1][0];
    newPosition[1] = mapElements.snake[mapElements.snake.length-1][1];
    newPosition = getNewPosition(newPosition, key);

    if(!isCorrectPosition(newPosition))
        lose = true;
    if(lose){
        endGame(false);
        return;
    }

    let exCase = mapElements.world[newPosition[1]][newPosition[0]];

    mapElements.world[newPosition[1]][newPosition[0]] = SNAKE;
    mapElements.snake.push(newPosition);

    let deleteSnakeCase = 0;
    let needChangeFoodPosition = false;
    if(exCase === FOOD){
        mapElements.score += 1;
        eatSound();

        needChangeFoodPosition = true;
        if(mapElements.food[0][2] === "shrink")
            deleteSnakeCase = 2;
    }else{
        deleteSnakeCase = 1;
    }

    while(deleteSnakeCase > 0 && mapElements.snake.length > 1){
        let removeElement = mapElements.snake.shift();
        // Savoir si il y a de la glace
        if(hasElement(mapOptions.ice, removeElement[0], removeElement[1]))
            mapElements.world[removeElement[1]][removeElement[0]] = ICE;
        else
            mapElements.world[removeElement[1]][removeElement[0]] = EMPTY;
        deleteSnakeCase--;
    }

    if(needChangeFoodPosition){
        if(!changeFoodPosition()){
            if(mapOptions.bonus > 0 && mapOptions.delay-mapOptions.bonus >= mapOptions.maxSpeed){
                mapOptions.delay = mapOptions.delay-mapOptions.bonus;
                clearInterval(gameListener);
                gameListener = setInterval(function(){
                    step();
                }, mapOptions.delay);
            }
        }
    }

    canChangeKey = true;
    drawGame();
}

/**
 * [GAME]
 * Pour choisir une nouvelle position de nourriture
 */
function changeFoodPosition(firstFood = false){
    let contains = false;
    for(let i=0; i<mapElements.world.length; i++){
        if(mapElements.world[i].includes(EMPTY) || mapElements.world[i].includes(ICE)){
            contains = true;
            break;
        }
    }
    if(!contains){
        endGame(true);
        return true;
    }

    let randomX = mapElements.snake[0][0];
    let randomY = mapElements.snake[0][1];
    while(mapElements.world[randomY][randomX] !== EMPTY && mapElements.world[randomY][randomX] !== ICE){
        randomX = Math.floor((Math.random() * mapElements.world[0].length));
        randomY = Math.floor((Math.random() * mapElements.world.length));
    }
    mapElements.world[randomY][randomX] = FOOD;

    let test = Math.floor((Math.random() * 4));
    let foodType = (test !== 0 ? "default" : "shrink");
    if(firstFood){
        foodType = mapOptions.defaultFood;
    }else{
        let randomFood = Math.floor((Math.random() * 101));
        if(randomFood < mapOptions.foodProbaDefault)
            foodType = "default";
        else
            foodType = "shrink";

    }

    mapElements.food[0] = [randomX, randomY, foodType];
    return false;
}

/**
 * [UTIL]
 * Pour avoir la potencielle nouvelle position de la tête du snake
 *
 * @param position L'ancienne position
 * @param pressedKey La touche selectionnée
 * @returns {*} La potencielle nouvelle position de la tête du snake
 */
function getNewPosition(position, pressedKey){
    if(pressedKey === "ArrowUp")
        position[1] = position[1]-1;
    else if(pressedKey === "ArrowDown")
        position[1] = position[1]+1;
    else if(pressedKey === "ArrowLeft")
        position[0] = position[0]-1;
    else if(pressedKey === "ArrowRight")
        position[0] = position[0]+1;
    return position;
}

/**
 * [UTIL]
 * Pour savoir si une position est accessible par le serpent
 *
 * @param position La positon à accéder
 * @returns {boolean} true si elle est accessible, false sinon
 */
function isCorrectPosition(position, cannonBall = false){
    if(cannonBall){
        return !(position[0] < 0 || position[0] >= mapElements.world[0].length || position[1] < 0 || position[1] >= mapElements.world.length ||
            mapElements.world[position[1]][position[0]] === WALL || mapElements.world[position[1]][position[0]] === CANNON);
    }

    return !(position[0] < 0 || position[0] >= mapElements.world[0].length || position[1] < 0 || position[1] >= mapElements.world.length ||
        mapElements.world[position[1]][position[0]] === SNAKE || mapElements.world[position[1]][position[0]] === WALL ||
        mapElements.world[position[1]][position[0]] === CANNON || mapElements.world[position[1]][position[0]] === CANNON_BALL);
}

/**
 * [GAME]
 * Lorsque la partie se termine
 *
 * @param victory Si le joueur a gagné
 */
function endGame(victory){
    clearInterval(gameListener);
    gameListener = null;
    key = null;
    canChangeKey = true;
    state = "menu";

    openMainPage();
    reloadMenu(victory);

    playSound(false);
    deathSound();
}





/**
 * [UTIL]
 * Lorsqu'on termine un redimensionnement de page
 */
function endOfScreenResizing(){
    let body = document.getElementsByTagName("body")[0];

    gameCanvas.height = document.documentElement.clientHeight;
    gameCanvas.width = body.clientWidth;
    if(state === "game" || (state === "menu" && mapElements.score != -1)){
        drawGame();
    }else if(state === "menu"){
        drawBackground();
    }else if(state === "editor"){
        drawEditor();
    }
    clearTimeout(lastResize);
    lastResize = null;
}








/**
 * [UTILS]
 * Pour avoir le parent qui contient tous les paramètres d'un paramètre
 *
 * @param element L'élément enfant
 * @param list La liste des options
 * @returns {null|*} L'élément HTML parent si il existe, null sinon
 */
function getCorrectParent(element, list){
    if(element === document)
        return null;

    let isGoodOption = false;
    for(let i=0; i<Object.values(list).length; i++){
        if(element === Object.values(list)[i].HTMLElement)
            isGoodOption = true;
    }
    if(!isGoodOption)
        return getCorrectParent(element.parentElement, list);
    return element;
}

/**
 * [UTILS]
 * Pour avoir l'élément option associé à un élément HTML
 *
 * @param id L'id de l'élément HTML
 * @param list La liste d'option dans laquelle chercher
 * @returns {null|*} L'option associée si elle existe, null sinon
 */
function getElementNameForId(id, list){
    for(let i=0; i<Object.values(list).length; i++){
        if(id === Object.values(list)[i].HTMLElement.id)
            return Object.values(list)[i].name;
    }
    return null;
}

