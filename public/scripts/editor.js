let editorHTML = {
    tools: {HTMLElement: null},
    config: {HTMLElement: null}
};


let editorElements;

let editorToolsElements = {
    wall: {HTMLElement: null, name: "wall"},
    ice: {HTMLElement: null, name: "ice"},
    snakeHead: {HTMLElement: null, name: "snakeHead"},
    snakeBody: {HTMLElement: null, name: "snakeBody"},
    food: {HTMLElement: null, name: "food"},
    cannon: {HTMLElement: null, name: "cannon"}
};

let editorToolsValue = "wall";

let editorClick = "left";
let editorButtonPressed = false;

/**
 * [LOAD]
 * Initialise les variables de l'editeur
 */
function createEditorVariables(){
    resetEditorValue();
    editorHTML.tools.HTMLElement = document.getElementById("editor-tools");
    editorHTML.config.HTMLElement = document.getElementById("editor-config");

    editorToolsElements.wall.HTMLElement = document.getElementById("editor-tools-wall");
    editorToolsElements.ice.HTMLElement = document.getElementById("editor-tools-ice");
    editorToolsElements.snakeHead.HTMLElement = document.getElementById("editor-tools-snake-head");
    editorToolsElements.snakeBody.HTMLElement = document.getElementById("editor-tools-snake-body");
    editorToolsElements.food.HTMLElement = document.getElementById("editor-tools-food");
    editorToolsElements.cannon.HTMLElement = document.getElementById("editor-tools-cannon");
}

/**
 * [LOAD]
 * Crée les évènements liés à l'éditeur
 */
function addEditorEvents(){
    dragElement(editorHTML.tools.HTMLElement);
    dragElement(editorHTML.config.HTMLElement);

    // Tools menu
    for(let i=0; i<Object.values(editorToolsElements).length; i++){
        let editorToolsElement = Object.values(editorToolsElements)[i].HTMLElement;
        if(editorToolsElement !== null)
            editorToolsElement.addEventListener("click", selectTool);
    }

    // Config button
    let configElement = document.getElementById("editor-config-config");
    configElement.addEventListener("click", configButton);

    // Food button
    let foodElement = document.getElementById("editor-config-food");
    foodElement.addEventListener("click", foodButton);

    // Export button
    let exportElement = document.getElementById("editor-config-export");
    exportElement.addEventListener("click", exportButton);

    // Save button
    let saveElement = document.getElementById("editor-config-save");
    saveElement.addEventListener("click", saveButton);

    // Go back button
    let backElement = document.getElementById("editor-config-back");
    backElement.addEventListener("click", backButton);


    // Editor click
    gameCanvas.addEventListener("click", clickMapEditor);
    gameCanvas.addEventListener("contextmenu",  rightClickMapEditor);

    // Editor hold button
    gameCanvas.addEventListener("mousedown", function (event) {
        let clickName = getClickedButtonName(event.which);
        if(state === "editor" && clickName != null){
            editorClick = clickName;
            editorButtonPressed = true;
        }
    });
    gameCanvas.addEventListener("mouseup", function (event) {
        let clickName = getClickedButtonName(event.which);
        if(state === "editor" && clickName === editorClick){
            editorButtonPressed = false;
        }
    });
    gameCanvas.addEventListener("mousemove", function (event) {
        if(state === "editor" && editorButtonPressed)
            changeCase(event, editorClick);
    });

}

/**
 * [EVENT]
 * Lorsqu'on click sur un outil dans le menu des outils
 *
 * @param event L'évènement de click
 */
function selectTool(event){
    let targetElement = getCorrectParent(event.target, editorToolsElements);
    if(targetElement === null)
        return;

    let tempElement = editorToolsElements[editorToolsValue].HTMLElement;
    if(tempElement !== null)
        tempElement.classList.remove("bg-light");
    editorToolsValue = getElementNameForId(targetElement.id, editorToolsElements);
    if(targetElement !== null)
        targetElement.classList.add("bg-light");
}


/**
 * [EVENT]
 * Lorsqu'on click sur le bouton de configuration
 *
 * @param event L'évènement de click
 */
function configButton(event){
    openConfigEditor();
}

/**
 * [EVENT]
 * Lorsqu'on click sur le bouton de nourriture
 *
 * @param event L'évènement de click
 */
function foodButton(event){
    openFoodEditor();
}

/**
 * [EVENT]
 * Lorsqu'on click sur le bouton d'export
 *
 * @param event L'évènement de click
 */
function exportButton(event){
    let message = "";
    if(editorElements.snake.length === 0){
        if(message !== "")
            message += "</br>"
        message += "Vous n'avez pas placé la tête du snake."
    }

    if(message !== ""){
        Swal.fire(
            "Exportation",
            message,
            'error'
        );
        return;
    }

    let name = editorElements.name+".json";
    let timeClick = new Date().getTime();

    let finalJson = getExportList();

    let exportLinkElement = document.getElementById("editor-config-export-link");
    let file = new Blob([JSON.stringify(finalJson, null, "  ")], {type: "application/json"});


    exportLinkElement.href = URL.createObjectURL(file);
    exportLinkElement.download = name;
    exportLinkElement.dataset.time = timeClick;

    setTimeout(function (event) {
        if(exportLinkElement.dataset.time == timeClick){
            exportLinkElement.removeAttribute("href");
            exportLinkElement.removeAttribute("download");
        }
    }, 10);
}

/**
 * [EVENT]
 * Pour sauvegarder la carte
 *
 * @param event L'évènement de clique
 */
function saveButton(event){
    let message = "";
    if(editorElements.snake.length === 0){
        if(message !== "")
            message += "</br>"
        message += "Vous n'avez pas placé la tête du snake."
    }

    if(message !== ""){
        Swal.fire(
            "Sauvegarde",
            message,
            'error'
        );
        return;
    }

    Swal.fire(
        "Sauvegarde",
        "Map sauvegardée avec succès !",
        'success'
    );

    saveUserMap(editorElements.name, getExportList());
}

/**
 * [EVENT]
 * Pour retourner à l'accueil
 *
 * @param event L'évènement de clique
 */
function backButton(event){
    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "Vous aller perdre ce que vous n'avez pas sauvegardé.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, retourner à l\'accueil',
        cancelButtonText: "Non"
    }).then((result) => {
        if (result.value) {
            loadMapInMenu();
            resetEditorValue();
            openMainPage();
        }
    });
}


/**
 * [EVENT]
 * Lorsqu'on click sur la map dans l'editeur de carte
 *
 * @param event L'évènement de click
 */
function clickMapEditor(event){
    if(state === "editor")
        changeCase(event);
}

/**
 * [EVENT]
 * Lorsqu'on click droit sur la map dans l'editeur de carte
 *
 * @param event L'évènement de click
 */
function rightClickMapEditor(event){
    event.preventDefault();
    if(state === "editor")
        changeCase(event, "right");
}

async function changeCase(event, click = "left"){
    let xCenter = gameCanvas.width/2;
    let yCenter = gameCanvas.height/2;

    let topX = xCenter-boxSize*(editorElements.world[0].length/2);
    let topY = yCenter-boxSize*(editorElements.world.length/2);
    let bottomX = xCenter+boxSize*(editorElements.world[0].length/2);
    let bottomY = yCenter+boxSize*(editorElements.world.length/2);

    let x = (event.x-topX)/boxSize | 0;
    let y = (event.y-topY)/boxSize | 0;

    if(x >= 0 && x<editorElements.width && y >= 0 && y < editorElements.height){

        // Calcul si on peut changer la case
        let canChange = true;
        if(editorElements.world[y][x] === SNAKE && editorElements.snake.length > 0){
            canChange = false;
            if(editorElements.snake[0][0] === x && editorElements.snake[0][1] === y){
                canChange = true;
            }else if(editorElements.snake[editorElements.snake.length-1][0] === x && editorElements.snake[editorElements.snake.length-1][1] === y){
                canChange = true;
            }
        }

        if(canChange){
            let updateLists = false;
            let snakeWidth = editorElements.snake.length;

            if(click === "left"){
                if(editorToolsValue === editorToolsElements.wall.name){
                    if(editorElements.world[y][x] === WALL)
                        return;
                    cutElements(x, y);

                    editorElements.world[y][x] = WALL;
                    editorElements.walls.push([x, y]);
                }else if(editorToolsValue === editorToolsElements.ice.name){
                    if(editorElements.world[y][x] === ICE)
                        return;
                    cutElements(x, y, false);

                    editorElements.world[y][x] = ICE;
                    editorElements.ice.push([x, y]);
                }else if(editorToolsValue === editorToolsElements.snakeHead.name){
                    if(editorElements.world[y][x] === SNAKE && editorElements.snake[snakeWidth-1][0] === x && editorElements.snake[snakeWidth-1][1] === y)
                        return;
                    cutElements(x, y, false);

                    while(editorElements.snake.length > 0){
                        let tempSnake = editorElements.snake[0];
                        resetCase(tempSnake[0], tempSnake[1]);
                        editorElements.snake.shift();
                    }

                    editorElements.world[y][x] = SNAKE;
                    editorElements.snake.push([x, y]);
                }else if(editorToolsValue === editorToolsElements.snakeBody.name){
                    if(editorElements.world[y][x] === SNAKE)
                        return;

                    if(snakeWidth > 0){
                        let lastSnakeX = editorElements.snake[0][0];
                        let lastSnakeY = editorElements.snake[0][1];

                        let headSnakeX = editorElements.snake[snakeWidth-1][0];
                        let headSnakeY = editorElements.snake[snakeWidth-1][1];

                        // On verifie qu'on ajoute un bout collé au snake
                        if(!( (Math.abs(x-lastSnakeX) === 1 && Math.abs(y-lastSnakeY) === 0) || (Math.abs(x-lastSnakeX) === 0 && Math.abs(y-lastSnakeY) === 1) )){
                            return;
                        }

                        // On vérifie qu'on ne bloque pas la tête du snake
                        let spaceTop = true, spaceRight = true, spaceBottom = true, spaceLeft = true;
                        if(headSnakeY-1 < 0 || editorElements.world[headSnakeY-1][headSnakeX] === SNAKE || (headSnakeY-1 === y && headSnakeX === x))
                            spaceTop = false;
                        if(headSnakeY+1 >= editorElements.height || editorElements.world[headSnakeY+1][headSnakeX] === SNAKE || (headSnakeY+1 === y && headSnakeX === x))
                            spaceBottom = false;
                        if(headSnakeX-1 < 0 || editorElements.world[headSnakeY][headSnakeX-1] === SNAKE || (headSnakeY === y && headSnakeX-1 === x))
                            spaceLeft = false;
                        if(headSnakeX+1 >= editorElements.width || editorElements.world[headSnakeY][headSnakeX+1] === SNAKE || (headSnakeY === y && headSnakeX+1 === x))
                            spaceRight = false;

                        if(!spaceTop && !spaceRight && !spaceBottom && !spaceLeft)
                            return;
                    }
                    cutElements(x, y, false);

                    editorElements.world[y][x] = SNAKE;
                    editorElements.snake.unshift([x, y]);
                }else if(editorToolsValue === editorToolsElements.food.name){
                    if(editorElements.world[y][x] === FOOD)
                        return;
                    cutElements(x, y, false);

                    // Supprimer l'ancienne nourriture
                    resetCase(editorElements.food[0], editorElements.food[1]);

                    editorElements.world[y][x] = FOOD;
                    editorElements.food = [x, y, editorElements.defaultFood];
                }else if(editorToolsValue === editorToolsElements.cannon.name){
                    editorButtonPressed = false;

                    const {value: formValues} = await Swal.fire({
                        title: 'Configuration du canon',
                        confirmButtonText: "Valider",
                        showCloseButton: true,
                        html:
                            '<div class="form-group mt-3">' +
                            '<label for="orientation">Sens du canon</label>' +
                            '<select class="form-control" id="orientation" aria-describedby="orientationHelp">' +
                            '<option value="top">Haut</option>' +
                            '<option value="right">Droite</option>' +
                            '<option value="bottom">Bas</option>' +
                            '<option value="left">Gauche</option>' +
                            '</select>'+
                            '<small id="orientationHelp" class="form-text text-muted"></small>' +
                            '</div>',
                        focusConfirm: false,
                        preConfirm: () => {
                            return [
                                document.getElementById('orientation') !== null ? document.getElementById('orientation').value : null
                            ]
                        },
                    });

                    if (formValues && formValues.length == 1) {
                        let orientation = formValues[0];
                        if (orientation !== undefined && orientation.length > 0){
                            cutElements(x, y, true);

                            editorElements.cannon.push([x, y, orientation]);
                            editorElements.world[y][x] = CANNON;
                        }
                    }
                }
            }else{
                cutElements(x, y);

                editorElements.world[y][x] = EMPTY;
            }
            drawEditor();
        }
    }
}

/**
 * Pour supprimer l'élément qu'il y avait sur la case
 *
 * @param x L'axe x de la case changée
 * @param y L'axe y de la case changée
 */
function resetCase(x, y){
    if(x === undefined || y == undefined)
        return;

    // Si il y a de la glace
    if(hasElement(editorElements.ice, x, y))
        editorElements.world[y][x] = ICE;
    else
        editorElements.world[y][x] = EMPTY;
}

/**
 * [UTIL]
 * Pour mettre à jour les listes des différents éléments de la carte lorsqu'on change une case
 *
 * @param x L'axe x de la case changée
 * @param y L'axe y de la case changée
 */
function cutElements(x, y, cutIce = true){
    let snakeWidth = editorElements.snake.length;
    if(editorElements.snake.length > 0 && editorElements.snake[0][0] === x && editorElements.snake[0][1] === y){
        editorElements.snake.shift();
    }else if(editorElements.snake.length > 0 && editorElements.snake[snakeWidth-1][0] === x && editorElements.snake[snakeWidth-1][1] === y){
        editorElements.snake.pop();
    }else if(editorElements.food.length > 0 && editorElements.food[0] === x && editorElements.food[1] === y){
        editorElements.food = [];
    }else if(editorElements.walls.length > 0 && editorElements.world[y][x] === WALL){
        for(let i=0; i<editorElements.walls.length; i++){
            if(editorElements.walls[i][0] === x && editorElements.walls[i][1] === y){
                editorElements.walls.splice(i, 1);
                break;
            }
        }
    }else if(editorElements.cannon.length > 0 && editorElements.world[y][x] === CANNON){
        for(let i=0; i<editorElements.cannon.length; i++){
            if(editorElements.cannon[i][0] === x && editorElements.cannon[i][1] === y){
                editorElements.cannon.splice(i, 1);
                break;
            }
        }
    }

    if(cutIce && editorElements.ice.length > 0){
        for(let i=0; i<editorElements.ice.length; i++){
            if(editorElements.ice[i][0] === x && editorElements.ice[i][1] === y){
                editorElements.ice.splice(i, 1);
                break;
            }
        }
    }
}

/**
 * [MENU]
 * Pour ouvrir le menu de configuration de la carte
 *
 * @returns {Promise<void>}
 */
async function openConfigEditor(){
    const {value: formValues} = await Swal.fire({
        title: 'Configuration de la carte',
        confirmButtonText: "Valider",
        showCloseButton: true,
        html:
            '<div class="form-group mt-3">' +
            '<label for="delay">Délais entre chaque mouvement (ms)</label>' +
            '<input type="number" class="form-control" id="delay" min="1" max="10000" aria-describedby="delayHelp" value="'+editorElements.delay+'">' +
            '<small id="delayHelp" class="form-text text-muted">Doit être compris entre 1 et 10000</small>' +
            '</div>'+
            '<div class="form-group">' +
            '<label for="bonus">Bonus de vitesse</label>' +
            '<input type="number" class="form-control" id="bonus" min="0" max="100" aria-describedby="bonusHelp" value="'+editorElements.bonus+'">' +
            '<small id="bonusHelp" class="form-text text-muted">Doit être compris entre 0 et 100</small>' +
            '</div>'+
            '<div class="form-group">' +
            '<label for="maxSpeed">Vitesse maximale</label>' +
            '<input type="number" class="form-control" id="maxSpeed" min="5" max="100" aria-describedby="maxSpeedHelp" value="'+editorElements.maxSpeed+'">' +
            '<small id="maxSpeedHelp" class="form-text text-muted">Doit être compris entre 5 et 100</small>' +
            '</div>',
        focusConfirm: false,
        preConfirm: () => {
            return [
                document.getElementById('delay') !== null ? document.getElementById('delay').value : null,
                document.getElementById('bonus') !== null ? document.getElementById('bonus').value : null,
                document.getElementById('maxSpeed') !== null ? document.getElementById('maxSpeed').value : null,
            ]
        },
    });

    let isValid = false;
    let verifyMessage = "";

    if (formValues && formValues.length == 3){
        let delay = parseInt(formValues[0]);
        let bonus = parseInt(formValues[1]);
        let maxSpeed = parseInt(formValues[2]);

        if(!isNaN(delay) && delay >= 1 && delay <= 10000){
            if(!isNaN(bonus) && bonus >= 0 && bonus <= 100){
                if(!isNaN(maxSpeed) && maxSpeed >= 5 && maxSpeed <= 100){
                    isValid = true;
                    editorElements.delay = delay;
                    editorElements.bonus = bonus;
                    editorElements.maxSpeed = maxSpeed;
                }else{
                    verifyMessage = "La vitesse maximale n'est pas comprise entre 5 et 100.";
                }
            }else{
                verifyMessage = "Le bonus n'est pas comprise entre 0 et 100.";
            }
        }else{
            verifyMessage = "Le délais n'est pas comprise entre 1 et 10000.";
        }
    }

    if(!isValid){
        if(verifyMessage !== ""){
            Swal.fire(
                'Erreur',
                verifyMessage,
                'error'
            );
        }
        return;
    }
}

/**
 * [MENU]
 * Pour ouvrir le menu de configuration de la nourriture
 *
 * @returns {Promise<void>}
 */
async function openFoodEditor(){
    // TODO
    // Update la couleur de la bouffe
    const {value: formValues} = await Swal.fire({
        title: 'Configuration de la nourriture',
        confirmButtonText: "Valider",
        showCloseButton: true,
        html:
            '<div class="form-group mt-3">' +
            '<label for="startFood">Nourriture de départ</label>' +
            '<select class="form-control" id="startFood" aria-describedby="startFoodHelp">' +
            '<option value="default" '+(editorElements.defaultFood === "default" ? "selected" : "")+'>Defaut</option>' +
            '<option value="shrink" '+(editorElements.defaultFood === "shrink" ? "selected" : "")+'>Rapetisse</option>' +
            '</select>'+
            '<small id="delayHelp" class="form-text text-muted"></small>' +
            '</div>'+
            '<h4 class="mt-4">Probabilitées</h4>'+
            '<div class="row">'+
            '<div class="form-group col-6">' +
            '<label for="probaDefault">Pomme normale</label>' +
            '<input type="number" class="form-control" id="probaDefault" min="0" max="100" aria-describedby="probaDefaultHelp" value="'+editorElements.foodProbaDefault+'">' +
            '<small id="probaDefaultHelp" class="form-text text-muted">Doit être compris entre 0 et 100</small>' +
            '</div>'+
            '<div class="form-group col-6">' +
            '<label for="probaShrink">Pomme rapetisse</label>' +
            '<input type="number" class="form-control" id="probaShrink" min="0" max="100" aria-describedby="probaShrinkHelp" value="'+editorElements.foodProbaShrink+'">' +
            '<small id="probaShrinkHelp" class="form-text text-muted">Doit être compris entre 0 et 100</small>' +
            '</div>'+
            '</div>',
        focusConfirm: false,
        preConfirm: () => {
            return [
                document.getElementById('startFood') !== null ? document.getElementById('startFood').value : null,
                document.getElementById('probaDefault') !== null ? document.getElementById('probaDefault').value : null,
                document.getElementById('probaShrink') !== null ? document.getElementById('probaShrink').value : null
            ]
        },
    });

    let isValid = false;
    let verifyMessage = "";

    if (formValues && formValues.length === 3){
        let startFood = formValues[0];
        let probaDefault = parseInt(formValues[1]);
        let probaShrink = parseInt(formValues[2]);

        if(startFood != null && startFood.length > 0){
            editorElements.defaultFood = startFood;
            if(editorElements.food.length > 0){
                editorElements.food[2] = startFood;
                drawEditor();
            }

            if(!isNaN(probaDefault) && probaDefault >= 0 && probaDefault <= 100){
                if(!isNaN(probaShrink) && probaShrink >= 0 && probaShrink <= 100){
                    if(probaDefault + probaShrink === 100){
                        editorElements.foodProbaDefault = probaDefault;
                        editorElements.foodProbaShrink = probaShrink;
                        isValid = true;
                    }else{
                        verifyMessage = "La sommes des probabilités des pommes n'est pas égale à 100.";
                    }
                }else{
                    verifyMessage = "La probabilité des pommes rapetissantes n'est pas comprise entre 0 et 100.";
                }
            }else{
                verifyMessage = "La probabilité des pommes normales n'est pas comprise entre 0 et 100.";
            }
        }else{
            verifyMessage = "Vous n'avez pas selectionné de type de nourriture de départ.";
        }
    }

    if(!isValid){
        if(verifyMessage !== ""){
            Swal.fire(
                'Erreur',
                verifyMessage,
                'error'
            );
        }
        return;
    }
}

/**
 * [UTIL]
 * Pour demander les paramètres de la carte
 *
 * @returns {Promise<void>}
 */
async function openDimensionEditor() {
    const {value: formValues} = await Swal.fire({
        title: 'Dimension de la carte',
        confirmButtonText: "Valider",
        showCloseButton: true,
        html:
            '<div class="form-group mt-3">' +
            '<label for="name">Nom</label>' +
            '<input class="form-control" id="name" aria-describedby="nameHelp" placeholder="Nom de la carte">' +
            '<small id="nameHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '<div class="form-group">' +
            '<label for="width">Largeur</label>' +
            '<input type="number" class="form-control" id="width" min="5" max="140" aria-describedby="widthHelp">' +
            '<small id="widthHelp" class="form-text text-muted">Doit être compris entre 5 et 140</small>' +
            '</div>' +
            '<div class="form-group">' +
            '<label for="height">Longueur</label>' +
            '<input type="number" class="form-control" id="height" min="5" max="80" aria-describedby="heightHelp">' +
            '<small id="heightHelp" class="form-text text-muted">Doit être compris entre 5 et 80</small>' +
            '</div>',
        focusConfirm: false,
        preConfirm: () => {
            return [
                document.getElementById('name') !== null ? document.getElementById('name').value : null,
                document.getElementById('width') !== null ? document.getElementById('width').value : null,
                document.getElementById('height') !== null ? document.getElementById('height').value : null
            ]
        },
    });

    let isValid = false;
    let verifyMessage = "";

    if (formValues){
        if(formValues.length === 3){
            let name = formValues[0];
            let width = parseInt(formValues[1]);
            let height = parseInt(formValues[2]);

            if(!isNaN(width) && width >= 5 && width <= 140){
                if(!isNaN(height) && height >= 5 && height <= 80){
                    if(name !== null && name.length > 0){
                        if(name.length <= 40){
                            isValid = true;
                            editorElements.width = width;
                            editorElements.height = height;
                            editorElements.name = name;
                            editorElements.world = Array.from(Array(height), () => Array.from(Array(width), () => EMPTY));
                            editorElements.snake = [];
                            editorElements.food = [];
                        }else{
                            verifyMessage = "Le nom ne doit pas excéder 40 caractères"
                        }
                    }else{
                        verifyMessage = "Vous n'avez pas rentré de nom."
                    }
                }else{
                    verifyMessage = "La largeur n'est pas comprise entre 5 et 80."
                }
            }else{
                verifyMessage = "La longueur n'est pas comprise entre 5 et 140.";
            }
        }else{
            verifyMessage = "Il n'y a pas le bon nombre de paramètres.";
        }
    }


    if(!isValid){
        if(verifyMessage !== ""){
            Swal.fire(
                'Erreur',
                verifyMessage,
                'error'
            );
        }
        openMainPage();
        return;
    }

    openEditorPage();
    drawEditor();
}

/**
 * [UTIL]
 * Pour afficher la zone d'édition
 * Quand : A chaque click et au lancement
 */
function drawEditor(){
    calculBoxSize(editorElements.width+2, editorElements.height+2);
    drawBackground();


    let xCenter = gameCanvas.width/2;
    let yCenter = gameCanvas.height/2;

    // La bordure de la zone de jeu
    let topX = xCenter-boxSize*(editorElements.width/2+borderSize);
    let topY = yCenter-boxSize*(editorElements.height/2+borderSize);
    let width = (editorElements.width+borderSize*2)*boxSize;
    let height = (editorElements.height+borderSize*2)*boxSize;
    gameContext.fillStyle = colors.wall;
    gameContext.fillRect(topX, topY, width, height);

    // La zone de jeu
    topX = xCenter-boxSize*(editorElements.width/2);
    topY = yCenter-boxSize*(editorElements.height/2);
    width = editorElements.width*boxSize;
    height = editorElements.height*boxSize;
    gameContext.fillStyle = colors.backgroundInside;
    gameContext.fillRect(topX, topY, width, height);

    for(let y=0; y<editorElements.world.length; y++){
        for(let x=0; x<editorElements.world[y].length; x++){
            let element = editorElements.world[y][x];
            if(element === WALL){
                gameContext.fillStyle = colors.wall;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
            }else if(element === ICE){
                gameContext.fillStyle = colors.ice;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
            }else if(element === FOOD){
                if(editorElements.food[2] === "default")
                    gameContext.fillStyle = colors.foodDefault;
                else if(editorElements.food[2] === "shrink")
                    gameContext.fillStyle = colors.foodShrink;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
            }else if(element === SNAKE){
                if(x === editorElements.snake[editorElements.snake.length-1][0] && y === editorElements.snake[editorElements.snake.length-1][1]) {
                    gameContext.fillStyle = colors.snakeHead;
                    gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);

                    gameContext.fillStyle = colors.backgroundOutisde;
                    let xScale = boxSize/5;
                    let yScale = boxSize/3;
                    gameContext.fillRect(topX+x*boxSize+xScale, topY+y*boxSize+yScale, xScale, yScale);
                    gameContext.fillRect(topX+x*boxSize+xScale*3, topY+y*boxSize+yScale, xScale, yScale);
                }
                else{
                    gameContext.fillStyle = colors.snakeBody;
                    gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);
                }
            }else if(element === CANNON){
                gameContext.fillStyle = colors.cannon;
                gameContext.fillRect(topX+x*boxSize, topY+y*boxSize, boxSize, boxSize);

                gameContext.fillStyle = colors.cannonText;
                gameContext.font = (boxSize*0.7)+"px Arial";
                gameContext.textAlign = "center";
                gameContext.textBaseline = "middle";

                let cannon = getCannon(editorElements.cannon, x, y);
                let letter = cannon === null ? "U" : getCannonLetter(cannon);

                gameContext.fillText(letter, topX+(x+0.5)*boxSize, topY+(y+0.5)*boxSize);
            }
        }
    }
}


// EXPORT :
// vérifier qu'il y a une head de serpent
// verifier que le joueur peut joueur avec cette disposition de head serpent
// verifier qu'il y a la food


// Enlever le state editor à la fin
// Cacher le tools et la config


/**
 * [UTIL]
 * Pour pouvoir déplacer le menu d'outils
 *
 * @param element Le menu d'outils
 */
function dragElement(element) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    element.onmousedown = dragMouseDown;

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;

        // set the element's new position:
        element.style.top = (element.offsetTop - pos2) + "px";
        element.style.left = (element.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

/**
 * [UTIL]
 * Pour avoir le nom du click associé au numéro du click event
 *
 * @param buttonNb Le numéro du click event
 * @returns {string|null} le nom du click si il existe, null sinon
 */
function getClickedButtonName(buttonNb){
    switch(buttonNb){
        case 1:
            return "left";
        case 3:
            return "right";
    }
    return null;
}

/**
 * [UTIL]
 * Pour mettre à 0 toutes les données de l'editeur
 */
function resetEditorValue() {
    editorElements = {
        width: 0,
        height: 0,

        delay: 200,
        bonus: 0,
        maxSpeed: 5,
        defaultFood: "default",
        foodProbaDefault: 90,
        foodProbaShrink: 10,

        name: "",
        world: [],
        snake: [],
        walls: [],
        ice: [],
        food: [],
        cannon: []
    };
}

function getExportList(){
    let finalJson = {
        "dimensions": [editorElements.width, editorElements.height],

        "delay": editorElements.delay,
        "bonus": editorElements.bonus,
        "maxSpeed": editorElements.maxSpeed,
        "foodProbaDefault": editorElements.foodProbaDefault,
        "foodProbaShrink": editorElements.foodProbaShrink,

        "walls": editorElements.walls,
        "ice": editorElements.ice,
        "food": [editorElements.food],
        "snake": editorElements.snake,
        "cannon": editorElements.cannon,
        "defaultFood": editorElements.defaultFood
    };
    return finalJson;
}
